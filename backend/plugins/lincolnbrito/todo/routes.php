<?php
header("Access-Control-Allow-Origin:*");
header("Access-Control-Allow-Headers:Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, Authorization");
header("Access-Control-Allow-Methods:OPTIONS,POST,GET,PUT,DELETE");
header("Access-Control-Allow-Credentials:true");
header('Accept: application/json');


use LincolnBrito\Todo\Models\Todo;
use Illuminate\Http\Request;

Route::get('api/populate', function(){
    $faker = Faker\Factory::create();

    for($i=0; $i<20; $i++) {
        Todo::create([
            'title' => $faker->sentence($nbWords=6, $variableNbWords = true),
            'description' => $faker->text($maxNbChars=200),
            'status' => $faker->boolean($chanceOfGettingTrue=50),
            'created_at' => $faker->date($format='Y-m-d H:i:s', $max='now')
        ]);
    }
});

Route::post('api/add-todo', function(Request $request){
    $data = $request->input();

    return Todo::create([
        'title' => $data['title'],
        'description' => $data['description'],
        'status' => $data['status']
    ]);

});


Route::post('api/delete-todo', function(Request $request){
    $data = $request->input();

    return Todo::destroy($data['id']);

});

Route::post('api/toggle-todo', function(Request $request){
    $data = $request->input();

    Todo::where('id',$data['id'])->update(['status' => $data['status']]);

});

Route::post('api/update-todo', function(Request $request){
    $data = $request->input();

    Todo::where('id',$data['id'])
        ->update([
            'status' => $data['status'],
            'title'  => $data['title'],
            'description' => $data['description']
        ]);
});

Route::get('/api/todos', function(){
    $todos = Todo::all();

    return $todos;
});


